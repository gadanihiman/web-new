import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage';

// import reducer here
import TodoReducers from './todo-example/reducer';
import LocaleReducers from './locales/reducer';

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['locales'],
}

const rootReducer = combineReducers({
  todos: TodoReducers,
  locales: LocaleReducers,
});

export default persistReducer(persistConfig, rootReducer)
