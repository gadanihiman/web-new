import { TYPE } from './action';

const initialState = {
  todoList: [],
  isLoading: false,
  error: null,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case TYPE.GET_TODO_REQUEST:
      return {
        ...state,
        isLoading: true,
      }
    case TYPE.GET_TODO_SUCCESS:
      return {
        ...state,
        isLoading: false,
        todoList: action.payload,
      }
    case TYPE.GET_TODO_ERROR:
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      }
    default:
      return state;
  }
}