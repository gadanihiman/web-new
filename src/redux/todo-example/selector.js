import { createSelector } from 'reselect';

const selectTodo = state => state.todos;

export const todoState = (state = 'todoList') => 
  createSelector(
    selectTodo,
    todos => todos[state],
  );
