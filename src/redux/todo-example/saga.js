import { fork, put, takeLatest } from 'redux-saga/effects';
import { TYPE, getTodoSuccess } from './action';

function* watchGetTodoRequest() {
  yield takeLatest(TYPE.GET_TODO_REQUEST, getTodoRequest)
}

function* getTodoRequest() {
  try {
    const result = yield getTodo();
    yield put(getTodoSuccess(result));
  } catch (error) {
    
  }
}

// example api request
const getTodo = async () => {
  return new Promise((resolve, reject) =>
    fetch('https://jsonplaceholder.typicode.com/todos?_limit=10')
      .then(response => response.json())
      .then(result => resolve(result))
      .catch(err => reject(err))
  );
}


const todoSaga = [
  fork(watchGetTodoRequest),
];

export default todoSaga;