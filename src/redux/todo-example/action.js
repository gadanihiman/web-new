export const TYPE = {
  GET_TODO_REQUEST: 'GET_TODO_REQUEST',
  GET_TODO_SUCCESS: 'GET_TODO_SUCCESS',
  GET_TODO_ERROR: 'GET_TODO_ERROR',
}

export const getTodoRequest = () => ({
  type: TYPE.GET_TODO_REQUEST,
})

export const getTodoSuccess = data => ({
  type: TYPE.GET_TODO_SUCCESS,
  payload: data,
})
