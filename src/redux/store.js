import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from './rootReducer';
import rootSaga from './rootSaga';
import { persistStore } from 'redux-persist';

const sagaMiddleware = createSagaMiddleware();

const bindMiddleware = middleware => {
  const middlewares = applyMiddleware(...middleware);
  if (process.env.NODE_ENV === 'development') {
    return composeWithDevTools(middlewares);
  }
  return compose(middlewares);
};

function configureStore(initialState = {}) {
  const store = createStore(
    rootReducer,
    initialState,
    bindMiddleware([sagaMiddleware]),
  );

  store.runSagaTask = () => {
    store.sagaTask = sagaMiddleware.run(rootSaga);
  };

  store.runSagaTask();
  const persistor = persistStore(store);
  
  return { store, persistor };
}

export default configureStore;
