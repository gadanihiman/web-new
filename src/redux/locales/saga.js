import { fork, put, takeLatest } from 'redux-saga/effects';
import { TYPE, changeLanguagesSuccess, changeLanguagesError } from './action';
import Translate from '../../utils/translate';

function* watchChangeLanguagesRequest() {
  yield takeLatest(TYPE.CHANGE_LANGUAGES_REQUEST, changeLanguagesRequest)
}

function* changeLanguagesRequest(action) {
  try {
    Translate.changeLanguage(action.payload.lang);
    yield put(changeLanguagesSuccess(action.payload));
  } catch (error) {
    yield put(changeLanguagesError(error));
  }
}


const localeSaga = [
  fork(watchChangeLanguagesRequest),
];

export default localeSaga;