import { TYPE } from './action';

const persistStorage = JSON.parse(localStorage.getItem('persist:root'))
const localesStorage = persistStorage && JSON.parse(persistStorage.locales);

const initialState = {
  currentLang: persistStorage ? localesStorage.currentLang : 'en',
  currentLangImg: persistStorage ? localesStorage.currentLangImg : require('../../images/uk.svg'),
  currentLangString: persistStorage ? localesStorage.currentLangString : 'ENGLISH',
  isLoading: false,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case TYPE.CHANGE_LANGUAGES_REQUEST:
      return {
        ...state,
        isLoading: true,
      }
    case TYPE.CHANGE_LANGUAGES_SUCCESS:
      const { lang, img, string } = action.payload;
      return {
        ...state,
        isLoading: false,
        currentLang: lang,
        currentLangString: string,
        currentLangImg: img,
      }
    case TYPE.CHANGE_LANGUAGES_ERROR: {
      return {
        ...state,
        isLoading: false,
      }
    }
    default:
      return state;
  }
}