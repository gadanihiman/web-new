export const TYPE = {
  CHANGE_LANGUAGES_REQUEST: "CHANGE_LANGUAGES_REQUEST",
  CHANGE_LANGUAGES_SUCCESS: "CHANGE_LANGUAGES_SUCCESS",
  CHANGE_LANGUAGES_ERROR: "CHANGE_LANGUAGES_ERROR"
}

export const changeLanguagesRequest = (languages) => ({
  type: TYPE.CHANGE_LANGUAGES_REQUEST,
  payload: languages,
})

export const changeLanguagesSuccess = (languages) => ({
  type: TYPE.CHANGE_LANGUAGES_SUCCESS,
  payload: languages,
})

export const changeLanguagesError = data => ({
  type: TYPE.CHANGE_LANGUAGES_SUCCESS,
  payload: data,
})
