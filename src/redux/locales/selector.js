import { createSelector } from 'reselect';

const selecLocale = state => state.locales;

export const localeState = (state = 'currentLang') => 
  createSelector(
    selecLocale,
    locales => locales[state],
  );
