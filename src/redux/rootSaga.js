import { all } from 'redux-saga/effects';

// import saga here 
import TodoSaga from './todo-example/saga';
import LocaleSaga from './locales/saga';

export default function* rootSaga() {
  yield all([
    ...TodoSaga,
    ...LocaleSaga,
  ]);
}
