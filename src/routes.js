import React from 'react';

// Import all components
const Main = React.lazy(() => import('./pages/Home/indexMain'));
const Agency = React.lazy(() => import('./pages/Agency/index'));
const Saas = React.lazy(() => import('./pages/Saas/index'));
const Apps = React.lazy(() => import('./pages/Apps/index'));
const Studio = React.lazy(() => import('./pages/Studio/index'));
const Business = React.lazy(() => import('./pages/Business/index'));
const Marketing = React.lazy(() => import('./pages/Marketing/index'));
const Hotel = React.lazy(() => import('./pages/Hotel/index'));
const ModernBusiness = React.lazy(() => import('./pages/ModernBusiness/index'));
const Coworking = React.lazy(() => import('./pages/Coworking/index'));
const CloudHosting = React.lazy(() => import('./pages/CloudHosting/index'));
const Event = React.lazy(() => import('./pages/Event/index'));
const Course = React.lazy(() => import('./pages/Course/index'));
const Personal = React.lazy(() => import('./pages/Personal/index'));
const SingleProduct = React.lazy(() => import('./pages/SingleProduct/index'));
const Enterprise = React.lazy(() => import('./pages/Enterprise/index'));
const Portfolio = React.lazy(() => import('./pages/Portfolio/index'));
const Services = React.lazy(() => import('./pages/Services/index'));
const ChangeLog = React.lazy(() => import('./pages/ChangeLog'));
const Components = React.lazy(() => import('./pages/Components'));
const Documentation = React.lazy(() => import('./pages/Documentation'));
const PageAboutUs = React.lazy(() => import('./pages/PageAboutUs'));
const PageBlog = React.lazy(() => import('./pages/PageBlog'));
const PageBlogDetail = React.lazy(() => import('./pages/PageBlogDetail'));
const PageBlogSidebar = React.lazy(() => import('./pages/PageBlogSidebar'));
const PageContactOne = React.lazy(() => import('./pages/PageContactOne'));
const PageContactThree = React.lazy(() => import('./pages/PageContactThree'));
const PageContactTwo = React.lazy(() => import('./pages/PageContactTwo'));
const PageJobApply = React.lazy(() => import('./pages/PageJobApply'));
const PageJobDetail = React.lazy(() => import('./pages/PageJobDetail'));
const PageJob = React.lazy(() => import('./pages/PageJob'));
const PagePricing = React.lazy(() => import('./pages/PagePricing'));
const PagePrivacy = React.lazy(() => import('./pages/PagePrivacy'));
const PageServices = React.lazy(() => import('./pages/PageServices'));
const PageTeam = React.lazy(() => import('./pages/PageTeam'));
const PageTerms = React.lazy(() => import('./pages/PageTerms'));
const PageWork = React.lazy(() => import('./pages/PageWork'));
const PageWorkDetail = React.lazy(() => import('./pages/PageWorkDetail'));
const MyCourse = React.lazy(() => import('./pages/MyCourse'));

const LandingPage = React.lazy(() => import('./pages/LandingPage'));


const routes = [

    // public Routes
    { path: '/template/index-saas', component: Saas },
    { path: '/template/index-apps', component: Apps },
    { path: '/template/index-agency', component: Agency },
    { path: '/template/index-studio', component: Studio },
    { path: '/template/index-business', component: Business },
    { path: '/template/index-marketing', component: Marketing },
    { path: '/template/index-hotel', component: Hotel },
    { path: '/template/index-modern-business', component: ModernBusiness },
    { path: '/template/index-coworking', component: Coworking },
    { path: '/template/index-cloud-hosting', component: CloudHosting },
    { path: '/template/index-event', component: Event },
    { path: '/template/index-personal', component: Personal },
    { path: '/template/index-single', component: SingleProduct },
    { path: '/template/index-enterprise', component: Enterprise },
    { path: '/template/index-portfolio', component: Portfolio },
    { path: '/template/index-services', component: Services },
    { path: '/template/changelog', component: ChangeLog },
    { path: '/template/components', component: Components },
    { path: '/template/documentation', component: Documentation },
    { path: '/template/page-aboutus', component: PageAboutUs },
    { path: '/template/page-blog', component: PageBlog },
    { path: '/template/page-blog-detail', component: PageBlogDetail },
    { path: '/template/page-blog-sidebar', component: PageBlogSidebar },
    { path: '/template/page-contact-one', component: PageContactOne },
    { path: '/template/page-contact-three', component: PageContactThree },
    { path: '/template/page-contact-two', component: PageContactTwo },
    { path: '/template/page-job-apply', component: PageJobApply },
    { path: '/template/page-job-detail', component: PageJobDetail },
    { path: '/template/page-job', component: PageJob },
    { path: '/template/page-pricing', component: PagePricing },
    { path: '/template/page-privacy', component: PagePrivacy },
    { path: '/template/page-services', component: PageServices },
    { path: '/template/page-team', component: PageTeam },
    { path: '/template/page-terms', component: PageTerms },
    { path: '/template/page-work', component: PageWork },
    { path: '/template/page-work-detail', component: PageWorkDetail },
    { path: '/template/index', component: Main },

    { path: '/course/detail', component: PageBlogDetail },
    { path: '/course', component: Course },
    { path: '/mycourse', component: MyCourse },
    { path: '/home', component: LandingPage }

];

export default routes;