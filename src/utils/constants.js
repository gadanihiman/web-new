export const BASE_API = '/api';
export const color = {
    white: '#FFFFFF',
    black: '#000000',
    orange: '#f5a462',
    pink: '#f85278',
    lightblue: '#5bb1ff',
    lightgreen: '#57b45a',
};
