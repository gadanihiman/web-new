import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import getStore from '../redux/store';

const { store } = getStore();
const resources = {
  en: { translation: require('../languages/en.json') },
  id: { translation: require('../languages/id.json') }
};

i18n
  .use(initReactI18next)
  .init({
    resources,
    fallbackLng: store.getState().locales.currentLang,
    debug: false,
    interpolation: {
      escapeValue: false,
    }
  });


export default i18n;