import React, { Component } from 'react';
import Layout from './components/Layout/';
import { Route, Switch, BrowserRouter as Router, withRouter, Redirect } from 'react-router-dom';

// Import Css
import './Apps.scss';
import './css/materialdesignicons.min.css';
import 'antd/dist/antd.css';

// Include Routes 
import routes from './routes';

// require translate
require('./utils/translate');

// Root Include
const Root = React.lazy(() => import('./pages/Home/indexRoot'));
const PageComingSoon = React.lazy(() => import('./pages/PageComingSoon'));
const PageContactDetail = React.lazy(() => import('./pages/PageContactDetail'));
const PageCoverLogin = React.lazy(() => import('./pages/PageCoverLogin'));
const PageCoverRePassword = React.lazy(() => import('./pages/PageCoverRePassword'));
const PageCoverSignup = React.lazy(() => import('./pages/PageCoverSignup'));
const PageError = React.lazy(() => import('./pages/PageError'));
const PageLogin = React.lazy(() => import('./pages/PageLogin'));
const PageMaintenance = React.lazy(() => import('./pages/PageMaintenance'));
const PageRecoveryPassword = React.lazy(() => import('./pages/PageRecoveryPassword'));
const PageSignup = React.lazy(() => import('./pages/PageSignup'));

function withLayout(WrappedComponent) {
  // ...and returns another component...
  return class extends React.Component {  
    render() {
      return <Layout>
        <WrappedComponent></WrappedComponent>
      </Layout>
    }
  };
}

class App extends Component {

  render() {

    return (
      <React.Fragment>  
      <Router>
          <React.Suspense fallback={<div></div>}>
            <Switch>
            {routes.map((route, idx) =>
                <Route path={route.path} component={withLayout(route.component)} key={idx} />
            )}
              <Route path="/template/page-comingsoon" component={PageComingSoon} />
              <Route path="/template/page-contact-detail" component={PageContactDetail} />
              <Route path="/template/page-cover-login" component={PageCoverLogin} />
              <Route path="/template/page-cover-signup" component={PageCoverSignup} />
              <Route path="/template/recover-password" component={PageCoverRePassword} />
              <Route path="/template" component={Root} />
              <Route path="/error" component={PageError} />
              <Route path="/maintenance" component={PageMaintenance} />
              <Route path="/recover-passowrd" component={PageRecoveryPassword} />
              <Route path="/signup" component={PageSignup} />
              <Route path="/login" component={PageLogin} />
              <Redirect exact from="/" to="/home" />
            </Switch>
          </React.Suspense>
        </Router>
      </React.Fragment>
    );
  }
}

export default withRouter(App);