import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from "react-router";
import styled from 'styled-components';
import en from '../../images/uk.svg';
import id from '../../images/id.svg';
import { withTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { localeState } from '../../redux/locales/selector';
import { changeLanguagesRequest } from '../../redux/locales/action';

const NavBarContainer = styled.div`
    padding: 0 1rem;
`;

const SearchListItem = styled.li`
    flex: 1 1 auto;
    display: flex !important;
    align-items: center;

    @media screen and (max-width: 991px) {
        display: none;
        order: 6;
        padding: 10px 20px 30px 28px;
    }
`;

const SearchInputWrapper = styled.div`
    height: 65%;
    width: 100%;
    border: 1px solid #dee2e6;
    border-radius: 10px;
    display: flex;
    align-items: center;
    flex-direction: row-reverse;
    input {
        color: var(--dark);
        font-weight: 600;
        width: 100%;
        height: 100%;
        padding-left: 16px;
        border: none;
        outline: none;
        box-shadow: none;
        background: transparent;

    }

    i {
        font-size: 24px;
        padding: 0 12px 0 8px;
        color: var(--dark);
    }

    @media screen and (max-width: 992px) {
        border-radius: 4px;
        height: 70%;
    }
    
`;

const LangImg = styled.img`
    width: 22px;
    margin-right: 6px;
`;

class Topbar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            page: false,
            landing: false,
            docs: false,
            new: false,
            utility: false,
            user: false,
            work: false,
            blog: false,
            carr: false
        };
        this.toggleLine = this.toggleLine.bind(this);
    }

    toggleLine() {
        this.setState(prevState => ({ isOpen: !prevState.isOpen }));
    }

    componentDidMount() {
        var matchingMenuItem = null;
        var ul = document.getElementById("top-menu");
        var items = ul.getElementsByTagName("a");
        for (var i = 0; i < items.length; ++i) {
            if (this.props.location.pathname === items[i].pathname) {
                matchingMenuItem = items[i];
                break;
            }
        }
        if (matchingMenuItem) {
            this.activateParentDropdown(matchingMenuItem);
        }
    }

    activateParentDropdown = (item) => {
        const parent = item.parentElement;
        if (parent) {
            parent.classList.add('active'); // li
            const parent1 = parent.parentElement;
            parent1.classList.add('active'); // li
            if (parent1) {
                const parent2 = parent1.parentElement;
                parent2.classList.add('active'); // li
                if (parent2) {
                    const parent3 = parent2.parentElement;
                    parent3.classList.add('active'); // li
                    if (parent3) {
                        const parent4 = parent3.parentElement;
                        parent4.classList.add('active'); // li
                    }
                }
            }
        }
    }

    changeLanguage = (data) => {
        this.props.changeLanguagesRequest(data);
        this.setState({ landing: !this.state.landing });
    }

    render() {
        const { t: translate } = this.props;
        return (
            <React.Fragment>
                <header id="topnav" className="defaultscroll sticky">
                    <NavBarContainer>
                        <div>
                            <Link className="logo" to="/">Alpha<span className="text-primary">.</span></Link>
                        </div>
                        <div className="buy-button">
                            <Link to="/signup" className="btn btn-primary">{translate('Topbar.register')}</Link>
                        </div>
                        <div className="menu-extras">
                            <div className="menu-item">
                                <Link to="#" onClick={this.toggleLine} className={this.state.isOpen ? "navbar-toggle open" : "navbar-toggle"} >
                                    <div className="lines">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </Link>
                            </div>
                        </div>

                        <div id="navigation" style={{ display: this.state.isOpen ? "block" : "none" }}>
                            <ul className="navigation-menu" id="top-menu">
                                <li>
                                    <ul className="navigation-menu">
                                        <li>
                                            <Link to="/">{translate('Topbar.home')}</Link>
                                        </li>
                                        <li className="has-submenu">
                                            <a href="javascript:void(0)" onClick={(event) => { event.preventDefault(); this.setState({ landing: !this.state.landing }) }}>{translate('Topbar.categories')}</a>
                                            <span className="menu-arrow"></span>
                                            <ul className={this.state.landing ? "submenu open" : "submenu"}  >
                                                <li><Link to="/template/index-saas">Persiapan Tes</Link></li>
                                                <li><Link to="/template/index-saas">Pengembangan Diri</Link></li>
                                                <li><Link to="/template/index-saas">Teknologi & Software</Link></li>
                                                <li><Link to="/template/index-saas">Bisnis & Keuangan</Link></li>
                                                <li><Link to="/template/index-saas">Pemasaran</Link></li>
                                                <li><Link to="/template/index-saas">Prakerja</Link></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <SearchListItem>
                                    <SearchInputWrapper>
                                        <i className="mdi mdi-magnify"></i>
                                        <input placeholder={translate('Topbar.searchCourse')} />
                                    </SearchInputWrapper>
                                </SearchListItem>
                                <li>
                                    <ul className="navigation-menu">
                                        <li className="has-submenu">
                                            <a href="javascript:void(0)" onClick={(event) => { event.preventDefault(); this.setState({ landing: !this.state.landing }) }}>
                                                <LangImg style={{ verticalAlign: "top" }} src={this.props.currentLangImg} alt="images" />
                                                {this.props.currentLangString}
                                            </a>
                                            <span className="menu-arrow"></span>
                                            <ul className={this.state.landing ? "submenu open" : "submenu"}>
                                                <li>
                                                    <Link to="#" onClick={() => this.changeLanguage({ lang: 'en', img: en, string: 'English' })}>
                                                        <LangImg src={en} alt="EN" />
                                                        {translate('Topbar.languages.english')}
                                                    </Link>
                                                </li>
                                                <li>
                                                    <Link to="#" onClick={() => this.changeLanguage({ lang: 'id', img: id, string: 'Bahasa' })}>
                                                        <LangImg src={id} alt="ID" />
                                                        {translate('Topbar.languages.bahasa')}
                                                    </Link>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <Link to="/login">{translate('Topbar.login')}</Link>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </NavBarContainer>
                </header>
            </React.Fragment>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    isLoading: localeState('isLoading'),
    currentLang: localeState('currentLang'),
    currentLangString: localeState('currentLangString'),
    currentLangImg: localeState('currentLangImg'),
});


const mapDispatchToProps = {
    changeLanguagesRequest,
}

export default withTranslation()(connect(mapStateToProps, mapDispatchToProps)(withRouter(Topbar)));
