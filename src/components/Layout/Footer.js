import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';

class Footer extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { t: translate } = this.props;
    return (
      <React.Fragment>
        <footer className="footer">
          <div className="container">
            <div className="row">
              <div className="col-lg-4 col-12 mb-0 mb-md-4 pb-0 pb-md-2">
                <Link className="logo-footer" to="#">Alpha<span className="text-primary">.</span></Link>
                <p className="mt-4">Start working with Alpha that can provide everything you need to generate awareness, drive traffic, connect.</p>
                <ul className="list-unstyled social-icon social mb-0 mt-4">
                  <li className="list-inline-item"><Link to="#" className="rounded mr-1"><i className="mdi mdi-instagram" title="Instagram"></i></Link></li>
                  <li className="list-inline-item"><Link to="#" className="rounded"><i className="mdi mdi-linkedin" title="Twitter"></i></Link></li>
                </ul>
              </div>

              <div className="col-lg-2 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <h4 className="text-light footer-head">{translate('Footer.about')}</h4>
                <ul className="list-unstyled footer-list mt-4">
                  <li><Link to="#" className="text-foot"><i className="mdi mdi-chevron-right mr-1"></i> {translate('Footer.aboutUs')}</Link></li>
                  <li><Link to="#" className="text-foot"><i className="mdi mdi-chevron-right mr-1"></i> {translate('Footer.help')}</Link></li>
                  <li><Link to="#" className="text-foot"><i className="mdi mdi-chevron-right mr-1"></i> {translate('Footer.becomePartner')}</Link></li>
                  <li><Link to="#" className="text-foot"><i className="mdi mdi-chevron-right mr-1"></i> {translate('Footer.contactUs')}</Link></li>
                </ul>
              </div>

              <div className="col-lg-3 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <h4 className="text-light footer-head">{translate('Footer.others')}</h4>
                <ul className="list-unstyled footer-list mt-4">
                  <li><Link to="#" className="text-foot"><i className="mdi mdi-chevron-right mr-1"></i> {translate('Footer.termsAndConditions')}</Link></li>
                  <li><Link to="#" className="text-foot"><i className="mdi mdi-chevron-right mr-1"></i> {translate('Footer.privacyPolicy')}</Link></li>
                  <li><Link to="#" className="text-foot"><i className="mdi mdi-chevron-right mr-1"></i> {translate('Footer.pressKit')}</Link></li>
                  <li><Link to="#" className="text-foot"><i className="mdi mdi-chevron-right mr-1"></i> {translate('Footer.promoTermsAndConditions')}</Link></li>
                </ul>
              </div>
              <div className="col-lg-3 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <h4 className="text-light footer-head">{translate('Footer.available')}</h4>
                <img src={require('../../images/googleplay.png')} alt="googleplay" className="img-fluid mx-auto mb-2 d-block" />
                <h4 className="text-light footer-head">{translate('Footer.comingSoon')}</h4>
                <img src={require('../../images/appstore.png')} alt="appstore" className="img-fluid mx-auto d-block" />
              </div>
            </div>
          </div>
        </footer>
        <hr />
        <footer className="footer footer-bar">
          <div className="container text-center">
            <div className="row align-items-center">
              <div className="col-sm-6">
                <div className="text-sm-left">
                  <p className="mb-0">©  {new Date().getFullYear()}  Alpha. All Rights Reserved.</p>
                </div>
              </div>
            </div>
          </div>
        </footer>

      </React.Fragment>
    );
  }
}

export default withTranslation()(Footer);
