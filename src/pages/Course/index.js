// React Basic and Bootstrap
import React, { useEffect } from 'react';
// import { Row } from 'reactstrap';

// Import Generic components
// import Feature from './Feature';
// import About from './About';
// import Cta from './Cta';
// import Cta1 from './Cta1';
import Services from './Services';
// import Testi from './Testi';
// import Partner from '../../components/Shared/Partner';
// import Home from './Home';


const Index = () => {
    const scrollNavigation = () => {
        const doc = document.documentElement;
        const top = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
        if (top > 80) {
            console.log('top > 80');
            document.getElementById('topnav').classList.add('nav-sticky');
        } else {
            console.log('top < 80');
            document.getElementById('topnav').classList.remove('nav-sticky');
        };
    }

    useEffect(() => {
        document.body.classList = "";
        document.getElementById('topnav').classList.add('bg-white');
        window.addEventListener("scroll", scrollNavigation, true);
    }, [window.addEventListener("scroll", scrollNavigation, true)]) // eslint-disable-line react-hooks/exhaustive-deps

    return (
        <>
            {/* <div id="preloader">
                    <div id="status">
                        <div className="spinner">
                            <div className="double-bounce1"></div>
                            <div className="double-bounce2"></div>
                        </div>
                    </div>
                </div> */}

                {/* Hero Start */}
                {/* <Home /> */}

                {/* Feature */}
                {/* <Feature /> */}

                {/* About */}
                {/* <About /> */}

                {/* Cta */}
                {/* <Cta /> */}

                {/* Services */}
                <Services />

                {/* Cta1 */}
                {/* <Cta1 /> */}

                {/* Testi */}
                {/* <Testi /> */}

                {/* Partner */}
                {/* <section className="bg-light section-two">
                    <div className="container">
                        <Row className="justify-content-center">
                            <Partner />
                        </Row>
                    </div>
                </section> */}

        </>
    );
};

export default Index;
