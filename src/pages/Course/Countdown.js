import React from 'react';
import styled from 'styled-components';
import Countdown from 'react-countdown';

const StyledCountdown = styled(Countdown)`
`;

const BaseCountdown = ({ ...config }) => {
    return (
        <div>
            <StyledCountdown
                date={Date.now() + 10e8}
                {...config}
            />
        </div>
    );
};

export default BaseCountdown;