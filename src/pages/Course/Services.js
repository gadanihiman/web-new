// React Basic and Bootstrap
import React from 'react';
import styled from 'styled-components';
import { Link, useHistory } from 'react-router-dom';
import { Row, Col } from 'reactstrap';
import Countdown from './Countdown';
import { color } from '../../utils/constants';

// import images
import course1 from '../../images/course/kursus1.jpg';
import course2 from '../../images/course/kursus2.jpg';
import course3 from '../../images/course/kursus3.jpg';
import course4 from '../../images/course/kursus4.jpg';
import course5 from '../../images/course/kursus5.jpg';
import course6 from '../../images/course/kursus6.jpg';
import course7 from '../../images/course/kursus7.jpg';
import course8 from '../../images/course/kursus8.jpg';
import course9 from '../../images/course/kursus9.jpg';
import imgFlash from '../../images/course/flash.png';

// import team01 from '../../images/team/1.jpg';
// import team02 from '../../images/team/2.jpg';
// import team03 from '../../images/team/3.jpg';
// import team04 from '../../images/team/4.jpg';

const Wrapper = styled.div`
    cursor: pointer;
`;

const CountdownWrapper = styled.div`
    background-color: ${color.orange};
    background-image: linear-gradient(to bottom right, ${color.pink}, ${color.orange});
    height: 50px;
    span {
        line-height: 50px;
        color: ${color.white};
        display: inline-block;
        float: right;
        margin: 0 20px;
    }
    margin: 0 0 20px;
    p {
        float: left;
        line-height: 50px;
        margin: 0 20px;
        text-transform: uppercase;
        font-weight: 900;
        color: ${color.white};
    }
    img {
        float: left;
        width: 40px;
        height: 40px;
        margin: 5px;
    }
`;

const Services = () => {
    const history = useHistory();
    return (
        <>
            <section className="section" id="courses">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-12 text-center">
                            <div className="section-title mb-4 pb-2">
                                <CountdownWrapper>
                                    <img src={imgFlash} alt="flash sale" /><p>flash sale</p><Countdown />
                                </CountdownWrapper>
                                <h4 className="title mb-4">Kelas Software dan Teknologi</h4>
                                <p className="text-muted para-desc mx-auto mb-0">Asah kemampuan teknologi & software dari para ahli.</p>
                            </div>
                        </div>
                    </div>

                    <Row>
                        <Col lg={4} md={6} className="col-12 mt-4 pt-2">
                            <div className="courses-desc position-relative overflow-hidden rounded border">
                                <Wrapper onClick={() => history.push('/course/detail')} className="position-relative d-block overflow-hidden">
                                    <img src={course1} className="img-fluid rounded-top mx-auto" alt="Landrick" />
                                    <div className="overlay-work"></div>
                                    <Link to="/course/detail" className="text-white h6 preview">Preview <i className="mdi mdi-chevron-right"></i></Link>
                                </Wrapper>
                                <div className="content p-3">
                                    <h5><Link to="/course/detail" className="title text-dark">Kelola Proyek dengan Microsoft dalam 2 Jam</Link></h5>
                                    <div className="rating">
                                        <ul className="list-unstyled mb-0">
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item">5 Bintang (3<i className="mdi mdi-account text-muted"></i>)</li>
                                        </ul>
                                    </div>
                                    <div className="fees">
                                        <ul className="list-unstyled float-right mb-0">
                                            <li className="h3"><span className="h6">Rp</span>75.000</li>
                                        </ul>
                                        <ul className="list-unstyled mb-0 numbers">
                                            <li><i className="mdi mdi-school text-muted"></i> 30 Peserta</li>
                                            <li><i className="mdi mdi-book text-muted"></i> 5 Sesi</li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="collection">
                                    <Link to="/course/detail"><i className="mdi mdi-heart h5"></i></Link>
                                </div>
                            </div>
                        </Col>

                        <Col lg={4} md={6} className="col-12 mt-4 pt-2">
                            <div className="courses-desc rounded border position-relative overflow-hidden">
                                <Wrapper onClick={() => history.push('/course/detail')} className="position-relative d-block overflow-hidden">
                                    <img src={course2} className="img-fluid rounded-top mx-auto" alt="Landrick" />
                                    <div className="overlay-work"></div>
                                    <Link to="/course/detail" className="text-white h6 preview">Preview <i className="mdi mdi-chevron-right"></i></Link>
                                </Wrapper >
                                <div className="content p-3">
                                    <h5><Link to="/course/detail" className="title text-dark">Dasar Statistik untuk Data Science (Pemula)</Link></h5>
                                    <div className="rating">
                                        <ul className="list-unstyled mb-0">
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star-outline h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item">3.99 Bintang (11<i className="mdi mdi-account text-muted"></i>)</li>
                                        </ul>
                                    </div>
                                    <div className="fees">
                                        <ul className="list-unstyled float-right mb-0">
                                            <li className="h3"><span className="h6">Rp</span>15.0000</li>
                                        </ul>
                                        <ul className="list-unstyled mb-0 numbers">
                                            <li><i className="mdi mdi-school text-muted"></i> 30 Peserta</li>
                                            <li><i className="mdi mdi-book text-muted"></i> 5 Sesi</li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="collection">
                                    <Link to="/course/detail"><i className="mdi mdi-heart h5"></i></Link>
                                </div>
                            </div>
                        </Col>

                        <Col lg={4} md={6} className="col-12 mt-4 pt-2">
                            <div className="courses-desc rounded border position-relative overflow-hidden">
                                <Wrapper onClick={() => history.push('/course/detail')} className="position-relative d-block overflow-hidden">
                                    <img src={course3} className="img-fluid rounded-top mx-auto" alt="Landrick" />
                                    <div className="overlay-work"></div>
                                    <Link to="/course/detail" className="text-white h6 preview">Preview <i className="mdi mdi-chevron-right"></i></Link>
                                </Wrapper >
                                <div className="content p-3">
                                    <h5><Link to="/course/detail" className="title text-dark">Dasar-dasar Mobile Programming Android</Link></h5>
                                    <div className="rating">
                                        <ul className="list-unstyled mb-0">
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star-half h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item">4.7 Bintang (9<i className="mdi mdi-account text-muted"></i>)</li>
                                        </ul>
                                    </div>
                                    <div className="fees">
                                        <ul className="list-unstyled float-right mb-0">
                                            <li className="h3"><span className="h6">Rp</span>130.000</li>
                                        </ul>
                                        <ul className="list-unstyled mb-0 numbers">
                                            <li><i className="mdi mdi-school text-muted"></i> 30 Peserta</li>
                                            <li><i className="mdi mdi-book text-muted"></i> 5 Sesi</li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="collection">
                                    <Link to="/course/detail"><i className="mdi mdi-heart h5"></i></Link>
                                </div>
                            </div>
                        </Col>

                        <Col lg={4} md={6} className="col-12 mt-4 pt-2">
                            <div className="courses-desc rounded border position-relative overflow-hidden">
                                <Wrapper onClick={() => history.push('/course/detail')} className="position-relative d-block overflow-hidden">
                                    <img src={course4} className="img-fluid rounded-top mx-auto" alt="Landrick" />
                                    <div className="overlay-work"></div>
                                    <Link to="/course/detail" className="text-white h6 preview">Preview <i className="mdi mdi-chevron-right"></i></Link>
                                </Wrapper >
                                <div className="content p-3">
                                    <h5><Link to="/course/detail" className="title text-dark">Tingkatkan Produktivitas dengan Microsoft Excel</Link></h5>
                                    <div className="rating">
                                        <ul className="list-unstyled mb-0">
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item">5 Bintang (3<i className="mdi mdi-account text-muted"></i>)</li>
                                        </ul>
                                    </div>
                                    <div className="fees">
                                        <ul className="list-unstyled float-right mb-0">
                                            <li className="h3"><span className="h6">Rp</span>850.000</li>
                                        </ul>
                                        <ul className="list-unstyled mb-0 numbers">
                                            <li><i className="mdi mdi-school text-muted"></i> 30 Peserta</li>
                                            <li><i className="mdi mdi-book text-muted"></i> 5 Sesi</li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="collection">
                                    <Link to="/course/detail"><i className="mdi mdi-heart h5"></i></Link>
                                </div>
                            </div>
                        </Col>

                        <Col lg={4} md={6} className="col-12 mt-4 pt-2">
                            <div className="courses-desc rounded border position-relative overflow-hidden">
                                <Wrapper onClick={() => history.push('/course/detail')} className="position-relative d-block overflow-hidden">
                                    <img src={course5} className="img-fluid rounded-top mx-auto" alt="Landrick" />
                                    <div className="overlay-work"></div>
                                    <Link to="/course/detail" className="text-white h6 preview">Preview <i className="mdi mdi-chevron-right"></i></Link>
                                </Wrapper >
                                <div className="content p-3">
                                    <h5><Link to="/course/detail" className="title text-dark">Belajar Tipografi dengan Adobe Illustrator</Link></h5>
                                    <div className="rating">
                                        <ul className="list-unstyled mb-0">
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item">5 Bintang (3<i className="mdi mdi-account text-muted"></i>)</li>
                                        </ul>
                                    </div>
                                    <div className="fees">
                                        <ul className="list-unstyled float-right mb-0">
                                            <li className="h3"><span className="h6">Rp</span>100.000</li>
                                        </ul>
                                        <ul className="list-unstyled mb-0 numbers">
                                            <li><i className="mdi mdi-school text-muted"></i> 30 Peserta</li>
                                            <li><i className="mdi mdi-book text-muted"></i> 5 Sesi</li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="collection">
                                    <Link to="/course/detail"><i className="mdi mdi-heart h5"></i></Link>
                                </div>
                            </div>
                        </Col>

                        <Col lg={4} md={6} className="col-12 mt-4 pt-2">
                            <div className="courses-desc rounded border position-relative overflow-hidden">
                                <Wrapper onClick={() => history.push('/course/detail')} className="position-relative d-block overflow-hidden">
                                    <img src={course6} className="img-fluid rounded-top mx-auto" alt="Landrick" />
                                    <div className="overlay-work"></div>
                                    <Link to="/course/detail" className="text-white h6 preview">Preview <i className="mdi mdi-chevron-right"></i></Link>
                                </Wrapper >
                                <div className="content p-3">
                                    <h5><Link to="/course/detail" className="title text-dark">Data Manajemen di Era Disrupsi Teknologi</Link></h5>
                                    <div className="rating">
                                        <ul className="list-unstyled mb-0">
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star-outline h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item">4 Bintang (2<i className="mdi mdi-account text-muted"></i>)</li>
                                        </ul>
                                    </div>
                                    <div className="fees">
                                        <ul className="list-unstyled float-right mb-0">
                                            <li className="h3"><span className="h6">Rp</span>109.000</li>
                                        </ul>
                                        <ul className="list-unstyled mb-0 numbers">
                                            <li><i className="mdi mdi-school text-muted"></i> 30 Peserta</li>
                                            <li><i className="mdi mdi-book text-muted"></i> 5 Sesi</li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="collection">
                                    <Link to="/course/detail"><i className="mdi mdi-heart h5"></i></Link>
                                </div>
                            </div>
                        </Col>

                        <Col lg={4} md={6} className="col-12 mt-4 pt-2">
                            <div className="courses-desc rounded border position-relative overflow-hidden">
                                <Wrapper onClick={() => history.push('/course/detail')} className="position-relative d-block overflow-hidden">
                                    <img src={course7} className="img-fluid rounded-top mx-auto" alt="Landrick" />
                                    <div className="overlay-work"></div>
                                    <Link to="/course/detail" className="text-white h6 preview">Preview <i className="mdi mdi-chevron-right"></i></Link>
                                </Wrapper >
                                <div className="content p-3">
                                    <h5><Link to="/course/detail" className="title text-dark">Agile dan Scrum dalam Development</Link></h5>
                                    <div className="rating">
                                        <ul className="list-unstyled mb-0">
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star-half h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item">4.5 Bintang (8<i className="mdi mdi-account text-muted"></i>)</li>
                                        </ul>
                                    </div>
                                    <div className="fees">
                                        <ul className="list-unstyled float-right mb-0">
                                            <li className="h3"><span className="h6">Rp</span>155.000</li>
                                        </ul>
                                        <ul className="list-unstyled mb-0 numbers">
                                            <li><i className="mdi mdi-school text-muted"></i> 30 Peserta</li>
                                            <li><i className="mdi mdi-book text-muted"></i> 5 Sesi</li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="collection">
                                    <Link to="/course/detail"><i className="mdi mdi-heart h5"></i></Link>
                                </div>
                            </div>
                        </Col>

                        <Col lg={4} md={6} className="col-12 mt-4 pt-2">
                            <div className="courses-desc rounded border position-relative overflow-hidden">
                                <Wrapper onClick={() => history.push('/course/detail')} className="position-relative d-block overflow-hidden">
                                    <img src={course8} className="img-fluid rounded-top mx-auto" alt="Landrick" />
                                    <div className="overlay-work"></div>
                                    <Link to="/course/detail" className="text-white h6 preview">Preview <i className="mdi mdi-chevron-right"></i></Link>
                                </Wrapper >
                                <div className="content p-3">
                                    <h5><Link to="/course/detail" className="title text-dark">Pengenalan Kubernetes dalam Pengembangan</Link></h5>
                                    <div className="rating">
                                        <ul className="list-unstyled mb-0">
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item">5 Bintang (3<i className="mdi mdi-account text-muted"></i>)</li>
                                        </ul>
                                    </div>
                                    <div className="fees">
                                        <ul className="list-unstyled float-right mb-0">
                                            <li className="h3"><span className="h6">Rp</span>135.000</li>
                                        </ul>
                                        <ul className="list-unstyled mb-0 numbers">
                                            <li><i className="mdi mdi-school text-muted"></i> 30 Peserta</li>
                                            <li><i className="mdi mdi-book text-muted"></i> 5 Sesi</li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="collection">
                                    <Link to="/course/detail"><i className="mdi mdi-heart h5"></i></Link>
                                </div>
                            </div>
                        </Col>

                        <Col lg={4} md={6} className="col-12 mt-4 pt-2">
                            <div className="courses-desc rounded border position-relative overflow-hidden">
                                <Wrapper onClick={() => history.push('/course/detail')} className="position-relative d-block overflow-hidden">
                                    <img src={course9} className="img-fluid rounded-top mx-auto" alt="Landrick" />
                                    <div className="overlay-work"></div>
                                    <Link to="/course/detail" className="text-white h6 preview">Preview <i className="mdi mdi-chevron-right"></i></Link>
                                </Wrapper >
                                <div className="content p-3">
                                    <h5><Link to="/course/detail" className="title text-dark">Kuasai Teknik Produksi Audio Digital</Link></h5>
                                    <div className="rating">
                                        <ul className="list-unstyled mb-0">
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item"><i className="mdi mdi-star h5 mb-0 text-warning"></i></li>
                                            <li className="list-inline-item">5 Bintang (3<i className="mdi mdi-account text-muted"></i>)</li>
                                        </ul>
                                    </div>
                                    <div className="fees">
                                        <ul className="list-unstyled float-right mb-0">
                                            <li className="h3"><span className="h6">Rp</span>555.000</li>
                                        </ul>
                                        <ul className="list-unstyled mb-0 numbers">
                                            <li><i className="mdi mdi-school text-muted"></i> 30 Peserta</li>
                                            <li><i className="mdi mdi-book text-muted"></i> 5 Sesi</li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="collection">
                                    <Link to="/course/detail"><i className="mdi mdi-heart h5"></i></Link>
                                </div>
                            </div>
                        </Col>

                        <Col className="mt-4 pt-2 text-center">
                            <Link to="/course/detail" className="btn btn-primary">See More Courses <i className="mdi mdi-chevron-right"></i></Link>
                        </Col>
                    </Row>
                </div>

                {/* Start instructor */}
                {/* <div className="container mt-100 mt-60" id="instructors">
                    <Row className="justify-content-center">
                        <Col className="text-center">
                            <div className="section-title mb-4 pb-2">
                                <h4 className="title mb-4">Instructors</h4>
                                <p className="text-muted para-desc mx-auto mb-0">Start working with <span className="text-primary font-weight-bold">Landrick</span> that can provide everything you need to generate awareness, drive traffic, connect.</p>
                            </div>
                        </Col>
                    </Row>

                    <Row>
                        <Col lg={3} md={6} className="mt-4 pt-2">
                            <div className="team text-center">
                                <div className="position-relative">
                                    <img src={team01} className="img-fluid d-block rounded-pill mx-auto" alt="Landrick" />
                                    <ul className="list-unstyled social-icon team-icon mb-0 mt-4">
                                        <li className="list-inline-item"><Link to="#" className="rounded"><i className="mdi mdi-facebook" title="Facebook"></i></Link></li>
                                        <li className="list-inline-item"><Link to="#" className="rounded"><i className="mdi mdi-instagram" title="Instagram"></i></Link></li>
                                        <li className="list-inline-item"><Link to="#" className="rounded"><i className="mdi mdi-twitter" title="Twitter"></i></Link></li>
                                        <li className="list-inline-item"><Link to="#" className="rounded"><i className="mdi mdi-google-plus" title="Twitter"></i></Link></li>
                                    </ul>
                                </div>
                                <div className="content pt-3 pb-3">
                                    <h5 className="mb-0"><Link to="#" className="name text-dark">Ronny Jofra</Link></h5>
                                    <small className="designation text-muted">Web Developers</small>
                                </div>
                            </div>
                        </Col>

                        <Col lg={3} md={6} className="mt-4 pt-2">
                            <div className="team text-center">
                                <div className="position-relative">
                                    <img src={team02} className="img-fluid d-block rounded-pill mx-auto" alt="Landrick" />
                                    <ul className="list-unstyled social-icon team-icon mb-0 mt-4">
                                        <li className="list-inline-item"><Link to="#" className="rounded"><i className="mdi mdi-facebook" title="Facebook"></i></Link></li>
                                        <li className="list-inline-item"><Link to="#" className="rounded"><i className="mdi mdi-instagram" title="Instagram"></i></Link></li>
                                        <li className="list-inline-item"><Link to="#" className="rounded"><i className="mdi mdi-twitter" title="Twitter"></i></Link></li>
                                        <li className="list-inline-item"><Link to="#" className="rounded"><i className="mdi mdi-google-plus" title="Twitter"></i></Link></li>
                                    </ul>
                                </div>
                                <div className="content pt-3 pb-3">
                                    <h5 className="mb-0"><Link to="#" className="name text-dark">Micheal Carlo</Link></h5>
                                    <small className="designation text-muted">Designer</small>
                                </div>
                            </div>
                        </Col>

                        <Col lg={3} md={6} className="mt-4 pt-2">
                            <div className="team text-center">
                                <div className="position-relative">
                                    <img src={team03} className="img-fluid d-block rounded-pill mx-auto" alt="Landrick" />
                                    <ul className="list-unstyled social-icon team-icon mb-0 mt-4">
                                        <li className="list-inline-item"><Link to="#" className="rounded"><i className="mdi mdi-facebook" title="Facebook"></i></Link></li>
                                        <li className="list-inline-item"><Link to="#" className="rounded"><i className="mdi mdi-instagram" title="Instagram"></i></Link></li>
                                        <li className="list-inline-item"><Link to="#" className="rounded"><i className="mdi mdi-twitter" title="Twitter"></i></Link></li>
                                        <li className="list-inline-item"><Link to="#" className="rounded"><i className="mdi mdi-google-plus" title="Twitter"></i></Link></li>
                                    </ul>
                                </div>
                                <div className="content pt-3 pb-3">
                                    <h5 className="mb-0"><Link to="#" className="name text-dark">Aliana Rosy</Link></h5>
                                    <small className="designation text-muted">UX / UI Designer</small>
                                </div>
                            </div>
                        </Col>

                        <Col lg={3} md={6} className="mt-4 pt-2">
                            <div className="team text-center">
                                <div className="position-relative">
                                    <img src={team04} className="img-fluid d-block rounded-pill mx-auto" alt="Landrick" />
                                    <ul className="list-unstyled social-icon team-icon mb-0 mt-4">
                                        <li className="list-inline-item"><Link to="#" className="rounded"><i className="mdi mdi-facebook" title="Facebook"></i></Link></li>
                                        <li className="list-inline-item"><Link to="#" className="rounded"><i className="mdi mdi-instagram" title="Instagram"></i></Link></li>
                                        <li className="list-inline-item"><Link to="#" className="rounded"><i className="mdi mdi-twitter" title="Twitter"></i></Link></li>
                                        <li className="list-inline-item"><Link to="#" className="rounded"><i className="mdi mdi-google-plus" title="Twitter"></i></Link></li>
                                    </ul>
                                </div>
                                <div className="content pt-3 pb-3">
                                    <h5 className="mb-0"><Link to="#" className="name text-dark">Sofia Razaq</Link></h5>
                                    <small className="designation text-muted">Ios Developer</small>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </div> */}
                {/* end instructor */}
            </section>
        </>
    );
};

export default Services;
