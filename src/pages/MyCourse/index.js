import React from 'react';
import styled from 'styled-components';
import { Tabs, Button as BaseButton, Row, Col } from 'antd';
import { color } from '../../utils/constants';

const Wrapper = styled.div`
    margin: 100px 50px;
`;

const InvoiceCard = styled.div`
    border: 1px solid ${color.lightblue};
    border-radius: 10px;
    padding: 20px;
    margin: 10px;
`;

const Light = styled.p`
    font-weight: 400;
    margin-bottom: 4px;
`;

const Bold = styled.p`
    font-weight: 700;
    margin-bottom: 4px;
`;

const Button = styled(BaseButton)`
    && {
        border-radius: 20px;
        ${({ green }) => green && `
            background-color: ${color.lightgreen};
            border: none;
            :hover {
                opacity: 0.8;
            }
        `}
    }
`;

const MyCourse = () => {
    console.log('MyCourse Page boy');
    const { TabPane } = Tabs;
    const date = new Date().toLocaleString();

    return (
        <>
            <Wrapper>
                <Tabs defaultActiveKey="1">
                    <TabPane tab="PEMBELIAN" key="1">
                        <Tabs tabPosition="left" defaultActiveKey="2">
                            <TabPane tab="Semua" key="4">
                                <InvoiceCard>
                                    <Row align="middle" justify="space-between">
                                        <Col span={18}>
                                            <Light>No. Invoice:</Light>
                                            <Bold>INV-3MMSKJAO</Bold>
                                            <br />
                                            <Bold>Pengembangan Diri</Bold>
                                            <Light>Tanggal pembelian: {date} </Light>
                                            
                                        </Col>
                                        <Col span={4}>
                                        <   Button type="primary" danger>Dibatalkan</Button>
                                        </Col>
                                    </Row>
                                </InvoiceCard>
                                <InvoiceCard>
                                    <Row align="middle" justify="space-between">
                                        <Col span={18}>
                                            <Light>No. Invoice:</Light>
                                            <Bold>INV-3MMSKJAO</Bold>
                                            <br />
                                            <Bold>Pengembangan Diri</Bold>
                                            <Light>Tanggal pembelian: {date} </Light>
                                            
                                        </Col>
                                        <Col span={4}>
                                        <   Button type="primary" green>Lunas</Button>
                                        </Col>
                                    </Row>
                                </InvoiceCard>
                            </TabPane>
                            <TabPane tab="Menunggu pembayaran" key="5">
                                Menunggu pembayaran
                            </TabPane>
                            <TabPane tab="Menunggu konfirmasi" key="6">
                                Menunggu konfirmasi
                            </TabPane>
                            <TabPane tab="Lunas" key="7">
                                Lunas
                            </TabPane>
                            <TabPane tab="Dibatalkan" key="8">
                                Dibatalkan
                            </TabPane>
                        </Tabs>
                    </TabPane>
                    <TabPane tab="KELAS SAYA" key="2">
                        <Tabs tabPosition="left" defaultActiveKey="9">
                            <TabPane tab="Semua" key="4">
                                Semuanya
                            </TabPane>
                            <TabPane tab="Dalam Progress" key="10">
                                Dalam Progress
                            </TabPane>
                            <TabPane tab="Selesai Seluruh Materi" key="11">
                                Selesai Seluruh Materi
                            </TabPane>
                            <TabPane tab="Lulus Exam" key="12">
                                Lulus Exam
                            </TabPane>
                        </Tabs>
                    </TabPane>
                    <TabPane tab="DAFTAR SERTIFIKAT" key="3">
                        <Tabs tabPosition="left" defaultActiveKey="13">
                            <TabPane tab="Semua" key="4">
                                Semuanya
                            </TabPane>
                            <TabPane tab="Selesai Seluruh Materi" key="15">
                                Selesai Seluruh Materi
                            </TabPane>
                            <TabPane tab="Lulus Exam" key="12">
                                Lulus Exam
                            </TabPane>
                        </Tabs>
                    </TabPane>
                </Tabs>
            </Wrapper>
        </>
    );
};

export default MyCourse;