// React Basic and Bootstrap
import React, { Component, useState } from 'react';
import { Link } from 'react-router-dom';
import { Row, Col, Card, CardBody, Collapse } from 'reactstrap';

// import images
import blog01 from '../images/blog/01.jpg';
import blog03 from '../images/blog/03.jpg';
import blog04 from '../images/blog/04.jpg';
import blog07 from '../images/blog/07.jpg';
import blog08 from '../images/blog/08.jpg';

// Client Images
import client1 from '../images/client/01.jpg';
import client2 from '../images/client/02.jpg';
import client3 from '../images/client/03.jpg';
import client4 from '../images/client/04.jpg';

const ContentCollapse = ({ sectionTitle, showCard = false, content = [] }) => {

  const [isOpen, setCollapse] = useState(showCard);

  return (
    <Card className="border mb-2">
      <Link className={isOpen ? "faq position-relative text-primary" : "faq position-relative text-dark"} onClick={() => setCollapse(!isOpen)} to="#">
        <div className="card-header bg-light p-3 d-flex">
          <i className={isOpen ? "mdi mdi-minus mr-2" : "mdi mdi-plus mr-2"}></i>
          <p className="font-weight-bold mb-0">{sectionTitle}</p>
          <p style={{ position: 'absolute', right: '3rem' }}>{content.length} Materi</p>
        </div>
      </Link>
      <Collapse isOpen={isOpen}>
        <CardBody className="mr-4">
          {content.map((item, index) => (
            <div key={index} className="d-flex justify-content-between">
              <p className="text-muted">{item.title}</p>
              <p className="text-muted justify-content-end">Link To Document/View</p>
            </div>
          ))}
        </CardBody>
      </Collapse>
    </Card>
  )
};

class PageBlogDetail extends Component {

  constructor(props) {
    super(props);
    this.state = {
      course: {
        title: 'Dasar Microsoft PowerPoint: Membuat PPT Bagi Pemula',
        desc: 'Dalam dunia kerja, presentasi bukan merupakan hal yang asing, bahkan termasuk salah satu kemampuan dasar yang harus dimiliki oleh seorang pekerja. Mulai dari menyampaikan ide di depan rekan-rekan kerja, hingga memperkenalkan produk baru kepada investor, semuanya dilakukan dengan presentasi. Presentasi pun tidak jauh kaitannya dengan PowerPoint. Kebanyakan presenter atau orang yang presentasi akan menggunakan PowerPoint untuk memvisualisasikan materi presentasi mereka. Namun, tidak banyak orang yang dapat membuat PPT yang baik dan menarik. Sebagai media yang menunjang presentasi, sudah seharusnya PPT yang kita buat dapat memperkuat penyampaian materi presentasi, serta menangkap perhatian audiens. Dalam kelas ini, talent Skill Academy akan membimbingmu untuk memaksimalkan .',
        tags: ['Sofware', 'Application'],
        price: '1.000.000',
        created_at: '13th August, 2019',
        created_by: 'Krista King',
        section: [
          {
            title: 'Introduction',
            content: [
              {
                title: 'Introduction',
                document: 'https://www.youtube.com/watch?v=I-QfPUz1es8',
              }
            ]
          },
          {
            title: 'Step Number 2',
            content: [
              {
                title: 'Introduction for step number 2',
                document: 'https://www.youtube.com/watch?v=I-QfPUz1es8',
              }
            ]
          },
          {
            title: 'Step Number 2',
            content: [
              {
                title: 'Introduction for step number 2',
                document: 'https://www.youtube.com/watch?v=I-QfPUz1es8',
              },
              {
                title: 'Introduction for step number 2',
                document: 'https://www.youtube.com/watch?v=I-QfPUz1es8',
              }
            ]
          }
        ]
      }
    }
  }

  componentDidMount() {
    document.body.classList = "";
    window.addEventListener("scroll", this.scrollNavigation, true);
  }

  // Make sure to remove the DOM listener when the component is unmounted.
  componentWillUnmount() {
    window.removeEventListener("scroll", this.scrollNavigation);
  }
  scrollNavigation = () => {
    var doc = document.documentElement;
    var top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
    if (top > 80) {
      document.getElementById('topnav').classList.add('nav-sticky');
    }
    else {
      document.getElementById('topnav').classList.remove('nav-sticky');
    }
  }

  render() {
    const { course } = this.state;

    return (
      <React.Fragment>
        <section className="bg-half bg-light">
          <div className="home-center">
            <div className="home-desc-center">
              <div className="container">
                <Row className="justify-content-center">
                  <Col lg={12} className="text-center">
                    <div className="page-next-level">
                      <h2>{course.title}</h2>
                      <ul className="list-unstyled mt-4">
                        <li className="list-inline-item h6 user text-muted mr-2"><i className="mdi mdi-account"></i>Created by {course.created_by} </li>
                        <li className="list-inline-item h6 date text-muted"><i className="mdi mdi-calendar-check"></i>{course.created_at}</li>
                      </ul>
                    </div>
                  </Col>
                </Row>
              </div>
            </div>
          </div>
        </section>

        <section className="section">
          <div className="container">
            <Row>
              <Col lg={8} md={7}>
                <div className="mr-lg-3">
                  <div className="blog position-relative overflow-hidden shadow rounded">
                    <div className="position-relative">
                      <img src={blog01} className="img-fluid rounded-top" alt="" />
                    </div>
                    <div className="content p-4">
                      <p className="text-muted mt-3">{course.desc}</p>
                      <h6>Topik yg dibahas :</h6>
                      <div className="mt-4">
                        <div className="accordion" id="accordionExample">
                          {this.state.course.section.map((item, index) => (
                            <ContentCollapse
                              sectionTitle={item.title}
                              showCard={index === 0}
                              content={item.content}
                              key={index}
                            />
                          ))}
                        </div>
                      </div>

                      <div className="mt-4">
                        <h6>Tentang Instruktur :</h6>
                        <div className="d-flex flex-column mt-4">
                          <div className="text-left p-1 mb-2">
                            <img src={client1} height="80" className="rounded-circle shadow float-left mr-3" alt="" />
                            <div className="content overflow-hidden d-block p-3 shadow rounded bg-white">
                              <ul className="list-unstyled mb-0">
                                <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                              </ul>
                              <p className="text-muted mt-2">" It seems that only fragments of the original text remain in the Lorem Ipsum texts used today. "</p>
                              <h6 className="text-primary">- Thomas Israel <small className="text-muted">C.E.O</small></h6>
                            </div>
                          </div>
                        </div>

                      </div>

                      <div className="post-meta mt-3">
                        <ul className="list-unstyled mb-0">
                          <li className="list-inline-item mr-2"><Link to="#" className="text-muted like"><i className="mdi mdi-heart-outline mr-1"></i>33</Link></li>
                          <li className="list-inline-item"><Link to="#" className="text-muted comments"><i className="mdi mdi-comment-outline mr-1"></i>08</Link></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </Col>

              <Col lg={4} md={5} className="col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <div className="sidebar mt-sm-30 p-4 rounded shadow">
                  <div className="widget mb-4 pb-2 d-flex flex-column">
                    <h4 className="widget-title">Course Details</h4>
                    <h1 className="text-center">Rp. <span className="text-danger text-bold">200.000</span></h1>
                    <h5 className="text-center text-success"><s className="text-muted">Rp {course.price}</s> Hemat 95%</h5>
                    <Link to="#" className="btn btn-primary rounded-pill mb-2"> <i className="mdi mdi-cart"> </i> Enroll Now </Link>
                  </div>

                  <div className="widget mb-4 pb-2">
                    <h4 className="widget-title">Kelas ini terdiri dari</h4>
                    <ul className="list-unstyled mt-2 mb-0 catagories">
                      <li><span><i className="mdi mdi-video" /> 10 Video</span></li>
                      <li><span><i className="mdi mdi-file" /> Document</span></li>
                      <li><span><i className="mdi mdi-lead-pencil" /> Exam</span></li>
                      <li><span><i className="mdi mdi-certificate" /> Sertifikat</span></li>
                    </ul>
                  </div>

                  <div className="widget mb-4 pb-2">
                    <h4 className="widget-title">Recent Post</h4>
                    <div className="mt-2">
                      <div className="clearfix post-recent">
                        <div className="post-recent-thumb float-left"> <Link to="#"> <img alt="img" src={blog07} className="img-fluid rounded" /></Link></div>
                        <div className="post-recent-content float-left"><Link to="#">Consultant Business</Link><span className="text-muted mt-2">15th June, 2019</span></div>
                      </div>
                      <div className="clearfix post-recent">
                        <div className="post-recent-thumb float-left"> <Link to="#"> <img alt="img" src={blog08} className="img-fluid rounded" /></Link></div>
                        <div className="post-recent-content float-left"><Link to="#">Look On The Glorious Balance</Link> <span className="text-muted mt-2">15th June, 2019</span></div>
                      </div>
                      <div className="clearfix post-recent">
                        <div className="post-recent-thumb float-left"> <Link to="#"> <img alt="img" src={blog01} className="img-fluid rounded" /></Link></div>
                        <div className="post-recent-content float-left"><Link to="#">Research Financial.</Link> <span className="text-muted mt-2">15th June, 2019</span></div>
                      </div>
                    </div>
                  </div>
                  {/* remove tag */}
                  {/* <div className="widget mb-4 pb-2">
                    <h4 className="widget-title">Tags</h4>
                    <div className="tagcloud mt-2">
                      {course.tags.map((item, i) => (
                        <Link to="#" key={i} className="rounded">{item}</Link>
                      ))}
                    </div>
                  </div> */}
                  {/* remove follow us */}
                  {/* <div className="widget">
                    <h4 className="widget-title">Follow us</h4>
                    <ul className="list-unstyled social-icon mt-2 mb-0">
                      <li className="list-inline-item"><Link to="#" className="rounded  mr-1"><i className="mdi mdi-facebook"></i></Link></li>
                      <li className="list-inline-item"><Link to="#" className="rounded mr-1"><i className="mdi mdi-instagram"></i></Link></li>
                      <li className="list-inline-item"><Link to="#" className="rounded mr-1"><i className="mdi mdi-twitter"></i></Link></li>
                      <li className="list-inline-item"><Link to="#" className="rounded mr-1"><i className="mdi mdi-vimeo"></i></Link></li>
                      <li className="list-inline-item"><Link to="#" className="rounded"><i className="mdi mdi-dribbble"></i></Link></li>
                    </ul>
                  </div> */}

                </div>
              </Col>
            </Row>
          </div>
        </section>
      </React.Fragment>
    );
  }
}
export default PageBlogDetail;
