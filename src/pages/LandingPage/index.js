// React Basic and Bootstrap
import React, { Component } from 'react';
import { Row } from 'reactstrap';

// Import Generic components
import Feature from './Feature';
import Services from './Services';
import Testi from './Testi';
import Partner from '../../components/Shared/Partner';
import Home from './Home';



class Index extends Component {

  

    componentDidMount() {
        document.body.classList = "";
        document.getElementById('topnav').classList.add('bg-white');
        window.addEventListener("scroll", this.scrollNavigation, true);
      }
  
       // Make sure to remove the DOM listener when the component is unmounted.
       componentWillUnmount() {
        window.removeEventListener("scroll",this.scrollNavigation);
     }
      scrollNavigation = () => {
          var doc = document.documentElement;
          var top = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
          if(top > 80)
          {
               document.getElementById('topnav').classList.add('nav-sticky');
          }
      }

    render() {

        return (
            <React.Fragment>
                <Home />
                <Feature />
                <Services />
                <Testi />

                <section className="bg-light section-two">
                    <div className="container">
                        <Row className="justify-content-center">
                        <Partner />
                        </Row>
                    </div>
                </section>

            </React.Fragment>
        );
    }
}

export default Index;
